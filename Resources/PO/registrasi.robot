*** Settings ***
Documentation    Registrasi
Library          Selenium2Library
Library          String
Library          Collections

*** Keywords ***
Verify User In web public
    Click Element             xpath:/html/body/header/nav/div/div[4]/ul/li[1]/a
    # Title Should Be               Indonesian Orthopaedic Association - Home
    Element Text Should Be        xpath:/html/body/section/div/div[1]/div[2]/div/div/div[1]/div/h1      Indonesian Orthopaedic Association

Click College
    Click Element       xpath:/html/body/header/nav/div/div[4]/ul/li[5]/a
    Wait Until Element Is Visible   xpath:/html/body/header/nav/div/div[4]/ul/li[5]/ul
    Click Element     xpath:/html/body/header/nav/div/div[4]/ul/li[5]/ul/li[7]/a

input Valid Email     xpath:/html/body/section/div[1]/div/div[1]/div/div/form/div[2]/div/input
    input text  