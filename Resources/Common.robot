*** Settings ***
Library    Selenium2Library

*** Variables ***
${delay}      0.2
${url}       https://dev.paboi.bigio.id/
${browser}    Chrome

*** Keywords ***
Begin Web Test
    ${chrome_options}=                    Evaluate             sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method                           ${chrome_options}    add_argument                                         test-type
    Call Method                           ${chrome_options}    add_argument                                         --no-sandbox
    # Call Method                           ${chrome_options}    add_argument                                         --headless
    Call Method                           ${chrome_options}    add_argument                                         --disable-dev-shm-usage
    Call Method                           ${chrome_options}    add_argument                                         --disable-extensions
    Call Method                           ${chrome_options}    add_argument                                         --disable-gpu
    Call Method                           ${chrome_options}    add_argument                                         start-maximized
    Open Browser                          ${url}               ${browser}                                           options=${chrome_options}
    Register Keyword To Run On Failure    NONE
    Set Window Size                       ${1920}              ${1080}
    Set Selenium Speed                    ${delay}
    Set Selenium Timeout                  10s

End Web Test
    Run Keyword If Test Failed    Capture Page Screenshot
    Close Browser

Navbar Title Should Be
    [Arguments]               ${title}
    Element Text Should Be    //*[@id="root"]/div[2]/div[3]/div/div[1]/div/div[1]    ${title}

Select List Single
    [Arguments]       ${locator}                ${value}
    Run Keyword If    "${value}"=="${EMPTY}"    Return From Keyword
    ...               ELSE                      Select Dropdown        ${locator}    ${value}

Select Dropdown
    [Arguments]                      ${locator}                                                    ${value}
    Click Element                    ${locator}
    Wait Until Element Is Visible    //*[contains(@class,"select__menu")]
    # Sleep                            1
    Wait Until Keyword Succeeds     1min        1sec        Click Element                    //div[contains(@class, "select__option") and .="${value}"]

Scroll Down
    Press Keys    NONE    \ue00f    # this is the Page Up key
    Sleep         1

Delete Filled Field
    [Arguments]    ${locator}
    Press Keys     //input[@name="${locator}"]    CTRL+a+BACKSPACE

Verify Modal Is Showed
    Wait Until Element Is Visible    xpath=(/html/body/div[2]/div/div[1])

Verify Modal Is Closed
    Page Should Not Contain Element    xpath=(/html/body/div[2]/div/div[1])

Verify Alert Success Text Showed Correctly
    [Arguments]                      ${message}
    Wait Until Element Is Visible    //*[contains(@class, "toast")]     20s
    # ${alert}                         Get Text                                                                   //*[contains(@class, "Toastify__toast-body")]
    Wait Until Element Is Visible        //*[contains(@class, "toast--success")]
    Wait Until Element Is Visible        //*[text()="${message}"]

Verify Alert Error Text Showed Correctly
    [Arguments]                      ${message}
    Wait Until Element Is Visible    //*[contains(@class, "toast")]     20s
    ${alert}                         Get Text                                                                   //*[contains(@class, "Toastify__toast-body")]
    Wait Until Element Is Visible        //*[contains(@class, "toast--error")]
    Wait Until Element Is Visible        //*[text()="${message}"]